import React from 'react';
import './App.css';
// import MUIDataTable from "mui-datatables";
// import data from './data.json';

// const columns = ["Employer name (Engligh", "僱主名稱 (中文)", "Wage subsidy amount (June - August 2020) / 工資補貼金額 (2020年6月至8月)", "Committed headcount under payroll /承諾受薪僱員人數"];


// const options = {
//   filterType: 'checkbox',
// };

function App() {
  return (
    
    <div className="App">
      {/* <header className="App-header">
      <MUIDataTable 
        title={"防疫抗疫基金已獲批名單"} 
        data={data} 
        columns={columns} 
        options={options} 
      />
      </header> */}
      <div>
        <p>Source: </p>
        <p> <a href='https://www.ess.gov.hk/zh/index.html' > 有關計劃</a> </p>
        <p> <a href='https://www.ess.gov.hk/zh/granted_companies.html' > 已獲批名單</a> </p>

      </div>
    </div>
  );
}

// const data = [["A & A FASHION HOUSE LIMITED", "",93000,4],["A & A OPTICAL", "",24000,1],["A & B HAIR BEAUTY SALON", "",28500,2],["A & B LOGISTICS LIMITED", "",10500,1],["A & J SOLUTIONS LTD","博客豪顧問有限公司",27000,1],["A & M TRAVEL & TOURS LIMITED","安逸旅遊有限公司",54000,2],["A & P LOGISTICS LTD","偉記物流運輸有限公司",196101,7],["A & R CORPORATE SECRETARIES LIMITED","安羅公司秘書有限公司",135000,5],["A & V PRODUCTION COMPANY LIMITED", "",81000,3],["A & W CONSULTANT CO.","A & W 顧問公司",22500,2],["A & Z HONG KONG LIMITED", "",18000,1],["A A PROPERTY SERVICES LIMITED","環亞物業顧問有限公司",270000,9],["A B CONCEPT","炫美創意",12000,1],["A BEAUTY COMPANY LIMITED", "",27000,1],["A BIG COMPANY LIMITED", "",108000,4],["A BLESSED WAY (HK) LTD.", "",530253,25],["A BOUTIQUE LIMITED", "",39000,2],["''A BRILLIANT WAY'' INTERNATIONAL LIMITED","閃耀國際有限公司",634947,27],["A C CITY LIMITED","冷氣城器材有限公司",47250,2],["A CONCEPT ART CONSULTANCY LIMITED","奕思藝術顧問有限公司",21030,1],["A CONCEPT GROUP LIMITED","吳家班投資有限公司",78000,3],["A DIAM LIMITED", "",127500,5],["A DIAMOND", "",27000,1],["A DROP OF LIFE LIMITED","點滴是生命有限公司",233850,9],["A FASHION COMPANY LIMITED","銳信時裝有限公司",378000,12],["A FOR ALAYO LIMITED","亞利奧有限公司",72750,3],["A FORMULA LIMITED","醇萃有限公司",23850,2],["A GALLERY LIMITED", "",10869,1],["A GO GO PROMOTIONS", "",182745,7],["A GOOD COMPANY LIMITED","壹間好公司有限公司",54000,2],["A GRACE ENTERPRISES LIMITED","美悅企業有限公司",27000,1],["A J COMPANY LIMITED","藝卓有限公司",189000,7],["A K INDUSTRIAL COMPANY LIMITED","雅基實業有限公司",289875,11],["A LEVEL RESOURCES LIMITED","匯豪資源有限公司",149400,6],["A LIFESTYLE", "",27000,1],["A LIST RACING LIMITED","一一一有限公司",54000,2],["A LITTLE SOMETHINK CO. LTD.", "",27000,3],["A LOCAL COMPANY LIMITED","一間本地有限公司",1950,2],["A MATCH PRODUCTION LIMITED", "",513471,19],["A MINOR PRODUCTION LIMITED","一點製作有限公司",42900,5],["A ONE BENZ LIMITED", "",22500,2],["A OPAL LIMITED","港暉商務有限公司",44250,3],["A PERFORMER LIMITED", "",42000,2],["A PLUS A TRADING LIMITED","優材貿易有限公司",27000,1],["A PLUS MONGKOK LIMITED", "",81000,3],["A PLUS SERVICES INTERNATIONAL LIMITED","柏翹國際有限公司",27000,1],["A PRIMO LIMITED","壹號有限公司",81000,3],["A PROMISE TRADING LIMITED","一諾貿易有限公司",15000,1],["A QUARTER TO NINE LTD.", "",27000,1],["A REALLY GOOD FILM COMPANY LIMITED","真好電影有限公司",54000,2],["A REALLY HAPPY FILM (HK) LIMITED","双喜電影發行(香港)有限公司",130200,5],["A SHUN & CO LTD", "",45000,3],["A SQUARE LIMITED", "",162000,6],["A STAR COCONUT LIMITED","其星椰子油有限公司",336351,19],["A TOP ACCOUNTING & SECRETARIAL LIMITED","會計秘書興業有限公司",54000,2],["A Y ENGINEERING CO","華億發工程公司",9000,1],["A Z TRADING CO", "",54000,2],["A&A (H.K.) INDUSTRIAL LIMITED","嘉泓(香港)實業有限公司",40500,2],["A&A BUILDING MATERIALS SUPPLIER","雅倫建築石料公司",216390,10],["A&A DEVELOPMENT CO","A&A發展公司",12000,1],["A&B DANCE ACADEMY", "",181500,7],["A&G CONSULTANCY LIMITED","天鉦專業顧問有限公司",18750,1],["A&J", "",47505,2],["A&R TECHNOLOGIES LIMITED","青嶸科技有限公司",37500,2],["A&S BROADCAST LTD", "",232059,9],["A. CHANG CPA & CO.","華進會計師樓",96000,5],["A. CONNECT LIMITED", "",81000,3],["A. G. KEEN DENTAL CLINIC LIMITED","奇恩健齒中心有限公司",54837,4],["A. H. PROPERTY LIMITED","帝后地產有限公司",26169,2],["A. INTEGRITY PROPERTY SERVICES LIMITED","力信佳物業服務有限公司",54000,2],["A. LI & ASSOCIATES ARCHITECTS LTD","李漢雄建築師樓有限公司",24900,1],["A. TESTONI HONG KONG LIMITED", "",873255,35],["A.AB.B GROUP LIMITED", "",77736,4],["A.D.E (HK) COMPANY LIMITED", "",27000,1],["A.E.DE FASHION LIMITED","A.E.歐日時裝批發有限公司",111906,5],["A.O. HORIZON LIMITED","傲豐金融科技有限公司",27000,1],["A.P. XPRESS (H.K.) LIMITED", "",913689,34],["A.PLUS MANAGEMENT CONSULTING ASIA PACIFIC LIMITED", "",123000,5],["A.R WORLDWIDE", "",111000,5],["A.SPEED LOGISTICS INT'L LIMITED","全達物流國際有限公司",54000,2],["A.W. FABER-CASTELL (H.K.) LIMITED","輝栢嘉(香港)有限公司",277137,12],["A/T FREE DESIGN", "",262065,12],["A1 DIGITAL", "",369000,14],["A168 LIMITED","A168 公司",341247,30],["A1818 LIMITED", "",180735,12],["A1M MANAGEMENT LIMITED", "",58200,3]];

export default App;